﻿using ManiwaCDM.Views;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace ManiwaCDM.Controls
{
    /// <summary>
    /// Interaction logic for McGridItem.xaml
    /// </summary>
    public partial class McGridItem : Button
    {
        int screenWidth;
        readonly MainView mainView;

        public McGridItem(MainView _mainView)
        {
            InitializeComponent();
            mainView = _mainView;

            SetupUI();
        }

        private void SetupUI()
        {
            if (mainView.WindowState != WindowState.Maximized) 
                screenWidth = (int)mainView.Width;
            else 
                screenWidth = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;

            int controlWidth = (screenWidth / 3) - 30;
            int column1 = controlWidth / 3;
            int Column2 = column1 * 2;
            Width = controlWidth;
            Height = Width / 2;

            mainGrid.Width = Width;

            gridCol1.Width = new GridLength(column1);
            gridCol2.Width = new GridLength(Column2);

            itemIcon.Width = column1;
            itemIcon.Height = column1;

            //ItemName.FontSize = 30;
            itemName.Padding = new Thickness(5);
        }

        public string Icon
        {
            get { return itemIcon.Source.ToString(); }
            set
            {
                Uri imageUri = new Uri(value, UriKind.Relative);
                BitmapImage imageBitmap = new BitmapImage(imageUri);
                itemIcon.Source = imageBitmap;
            }
        }
        public string Title
        {
            get { return itemName.Text; }
            set { itemName.Text = value; }
        }

        public int Itemid { get; internal set; }
    }
}
