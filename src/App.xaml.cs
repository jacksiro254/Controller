﻿using ManiwaCDM.Views;
using System.Globalization;
using System.Windows;

namespace ManiwaCDM
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        private void AppStartup(object sender, StartupEventArgs e)
        {
            FrameworkElement.LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(
                    System.Windows.Markup.XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            //TechCore.WriteLogs("App Events", "ManiwaCDM Opened", "", "");

            MainView mainView = new MainView();
            mainView.Show();
        }

        private void AppExit(object sender, ExitEventArgs e)
        {

        }
    }
}
