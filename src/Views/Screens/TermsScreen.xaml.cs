﻿using ManiwaCDM.Utils;
using System.Windows;
using System.Windows.Controls;

namespace ManiwaCDM.Views.Screens
{
    /// <summary>
    /// Interaction logic for TermsScreen.xaml
    /// </summary>
    public partial class TermsScreen : UserControl
    {
        readonly MainView mainView;

        public TermsScreen(MainView _mainView)
        {
            InitializeComponent();
            mainView = _mainView;
        }

        public string Title
        {
            get { return title.Text; }
            set { title.Text = value; }
        }

        public string Details
        {
            get { return details.Text; }
            set { details.Text = value; }
        }

        public void ManageView()
        {
            Title = AppUtils.TermsAndConditions;
            Details = AppUtils.TermsAndConditionsText;

            Visibility = Visibility.Visible;
            mainView.homePage.Visibility = Visibility.Collapsed;
            mainView.disclaimerScreen.Visibility = Visibility.Collapsed;
            mainView.accountScreen.Visibility = Visibility.Collapsed;
            mainView.narrationScreen.Visibility = Visibility.Collapsed;
            mainView.verifyScreen.Visibility = Visibility.Collapsed;
            mainView.countScreen.Visibility = Visibility.Collapsed;
            mainView.transactionScreen.Visibility = Visibility.Collapsed;
            mainView.finishScreen.Visibility = Visibility.Collapsed;
            mainView.ChangeRightButtonToDarkOrange(AppUtils.BtnContinue);
            mainView.btnRight.IsEnabled = true;
        }
    }
}
