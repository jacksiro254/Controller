﻿using ManiwaCDM.Controls;
using ManiwaCDM.Data.Models;
using ManiwaCDM.Services;
using ManiwaCDM.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace ManiwaCDM.Views.Screens
{
    /// <summary>
    /// Interaction logic for HomeScreen.xaml
    /// </summary>
    public partial class HomeScreen : UserControl
    {
        public List<McGridItem> items;
        public List<McGridItem> mcGridItems;
        readonly MainView mainView;

        int connects;

        public HomeScreen(MainView _mainView)
        {
            InitializeComponent();
            mainView = _mainView;
        }

        public void LoadTransactionTypes()
        {
            items = new List<McGridItem>() {
                new McGridItem(mainView){ Title = "Local Kes Account", Icon="../Resources/ncba.png"},
                new McGridItem(mainView){ Title = "M-Pesa Super Agency", Icon="../Resources/mpesa.png"},
                new McGridItem(mainView){ Title = "Brookside", Icon="../Resources/brookside.png"},
                new McGridItem(mainView){ Title = "M-Pesa Holding", Icon="../Resources/mpesa.png"},
                new McGridItem(mainView){ Title = "Loop Deposit", Icon="../Resources/loop.png"},
            };

            Setup(items);
            mainView.mcLoading.IsActive = false;
        }

        public void SetupFields()
        {
            connects = connects + 1;
        }

        public void Setup(List<McGridItem> _mcGridItems)
        {
            mcGridItems = _mcGridItems;
            foreach (McGridItem McGridItem in mcGridItems)
            {
                McGridItem.Margin = new Thickness(5);
                Items.Children.Add(McGridItem);
                McGridItem.Click += McGridItem_Click;
            }
        }

        private void McGridItem_Click(object sender, RoutedEventArgs e)
        {
            McGridItem Item = sender as McGridItem;
            mainView.selected = Item;
            mainView.currentScreen = 1;
            mainView.SetCurrentScreen();
        }
        public string Title
        {
            get { return PanelTitle.Text; }
            set { PanelTitle.Text = value; }

        }

        public void ManageView()
        {
            connects = connects + 1;
            Visibility = Visibility.Collapsed;
            mainView.disclaimerScreen.Visibility = Visibility.Collapsed;
            mainView.termsScreen.Visibility = Visibility.Collapsed;
            mainView.accountScreen.Visibility = Visibility.Collapsed;
            mainView.narrationScreen.Visibility = Visibility.Collapsed;
            mainView.verifyScreen.Visibility = Visibility.Collapsed;
            mainView.countScreen.Visibility = Visibility.Collapsed;
            mainView.transactionScreen.Visibility = Visibility.Collapsed;
            mainView.finishScreen.Visibility = Visibility.Collapsed;
            mainView.ChangeRightButtonToDodgerBlue();
            mainView.btnRight.IsEnabled = true;

            mainView.btnLeft.Visibility = Visibility.Hidden;
            mainView.btnRight.Visibility = Visibility.Hidden;
            mainView.mcLoading.IsActive = true;

            AsyncStatusReport asyncStatusReport = new AsyncStatusReport(mainView.socket);
            asyncStatusReport.Completed += StatusCheckerCompleted;

            try
            {
                Console.WriteLine("Checking status ...");
                asyncStatusReport.StartAsync();
            }
            catch (Exception)
            {
                asyncStatusReport.Dispose();
            }
        }

        public void StatusCheckerCompleted(object sender, RunWorkerCompletedEventArgs args)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            worker.Dispose();

            StatusReport status = (StatusReport)args.Result;

            Console.WriteLine("Controller Status: " + status.ControllerState);
            if (status.ControllerState == ApiUtils.ControllerState.IDLE.ToString())
            {
                mainView.btnLeft.Visibility = Visibility.Visible;
                mainView.btnRight.Visibility = Visibility.Visible;
                mainView.mcLoading.IsActive = false;
                Visibility = Visibility.Visible;
            }
            else
            {
                if (connects < 3)
                {
                    Console.WriteLine("Rechecking Controller Status");
                    ManageView();
                }
            }
        }
        
    }
}
