﻿using ManiwaCDM.Utils;
using System.Windows;
using System.Windows.Controls;

namespace ManiwaCDM.Views.Screens
{
    /// <summary>
    /// Interaction logic for TransactionScreen.xaml
    /// </summary>
    public partial class FinishScreen : UserControl
    {
        readonly MainView mainView;

        public FinishScreen(MainView _mainView)
        {
            InitializeComponent();
            mainView = _mainView;
        }
        public string Title
        {
            get { return title.Text; }
            set { text.Text = value; }
        }

        public string Text
        {
            get { return text.Text; }
            set { text.Text = value; }
        }

        public void ManageView()
        {
            Visibility = Visibility.Collapsed;
            mainView.disclaimerScreen.Visibility = Visibility.Collapsed;
            mainView.termsScreen.Visibility = Visibility.Collapsed;
            mainView.accountScreen.Visibility = Visibility.Collapsed;
            mainView.narrationScreen.Visibility = Visibility.Collapsed;
            mainView.verifyScreen.Visibility = Visibility.Collapsed;
            mainView.countScreen.Visibility = Visibility.Collapsed;
            mainView.transactionScreen.Visibility = Visibility.Collapsed;
            mainView.finishScreen.Visibility = Visibility.Collapsed;
            mainView.ChangeRightButtonToDarkOrange(AppUtils.BtnFinish);
            mainView.btnRight.IsEnabled = true;
        }
    }
}
