﻿using ManiwaCDM.Utils;
using System.Windows;
using System.Windows.Controls;

namespace ManiwaCDM.Views.Screens
{
    /// <summary>
    /// Interaction logic for DisclaimerScreen.xaml
    /// </summary>
    public partial class DisclaimerScreen : UserControl
    {
        readonly MainView mainView;

        public DisclaimerScreen(MainView _mainView)
        {
            InitializeComponent();
            mainView = _mainView;
        }

        public string Title
        {
            get { return title.Text; }
            set { title.Text = value; }
        }

        public string Details
        {
            get { return details.Text; }
            set { details.Text = value; }
        }

        public void ManageView()
        {
            Title = AppUtils.Disclaimer;
            Details = AppUtils.DisclaimerText;

            Visibility = Visibility.Visible;
            mainView.homePage.Visibility = Visibility.Collapsed;
            mainView.termsScreen.Visibility = Visibility.Collapsed;
            mainView.accountScreen.Visibility = Visibility.Collapsed;
            mainView.narrationScreen.Visibility = Visibility.Collapsed;
            mainView.verifyScreen.Visibility = Visibility.Collapsed;
            mainView.countScreen.Visibility = Visibility.Collapsed;
            mainView.transactionScreen.Visibility = Visibility.Collapsed;
            mainView.finishScreen.Visibility = Visibility.Collapsed;
            mainView.ChangeRightButtonToDarkOrange(AppUtils.BtnContinue);
            mainView.btnRight.IsEnabled = true;

        }

    }
}
