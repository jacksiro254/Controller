﻿using ManiwaCDM.Data.Models;
using ManiwaCDM.Services;
using ManiwaCDM.Utils;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace ManiwaCDM.Views.Screens
{
    /// <summary>
    /// Interaction logic for NarrationScreen.xaml
    /// </summary>
    public partial class NarrationScreen : UserControl
    {
        readonly MainView mainView;
        int FieldFocus, connects;

        public NarrationScreen(MainView _mainView)
        {
            InitializeComponent();
            mainView = _mainView;
            SetupFields();
        }

        public void SetupFields()
        {
            Narration = narrationInput.Text = "";
            YourName = yourNameInput.Text = "";
            PhoneNumber = phoneNumberInput.Text = "";
            IdNumber = idNumberInput.Text = "";
            mainView.narration = Narration;
            mainView.yourName = YourName;
            mainView.phoneNumber = PhoneNumber;
            mainView.idNumber = IdNumber;

            connects = connects + 1;
            FieldFocus = 0;
            SetFieldFocus();
        }

        public string Narration
        {
            get { return narrationInput.Text; }
            set { narrationInput.Text = value; }
        }

        public string YourName
        {
            get { return yourNameInput.Text; }
            set { yourNameInput.Text = value; }
        }

        public string PhoneNumber
        {
            get { return phoneNumberInput.Text; }
            set { phoneNumberInput.Text = value; }
        }

        public string IdNumber
        {
            get { return idNumberInput.Text; }
            set { idNumberInput.Text = value; }
        }

        private void EnableNumericKeys(bool enable)
        {
            btn0.IsEnabled = enable;
            btn1.IsEnabled = enable;
            btn2.IsEnabled = enable;
            btn3.IsEnabled = enable;
            btn4.IsEnabled = enable;
            btn5.IsEnabled = enable;
            btn6.IsEnabled = enable;
            btn7.IsEnabled = enable;
            btn8.IsEnabled = enable;
            btn9.IsEnabled = enable;
        }

        private void EnableAlphabeticalKeys(bool enable)
        {
            btnA.IsEnabled = enable;
            btnB.IsEnabled = enable;
            btnC.IsEnabled = enable;
            btnD.IsEnabled = enable;
            btnE.IsEnabled = enable;
            btnF.IsEnabled = enable;
            btnG.IsEnabled = enable;
            btnH.IsEnabled = enable;
            btnI.IsEnabled = enable;
            btnJ.IsEnabled = enable;
            btnK.IsEnabled = enable;
            btnL.IsEnabled = enable;
            btnM.IsEnabled = enable;
            btnN.IsEnabled = enable;
            btnO.IsEnabled = enable;
            btnP.IsEnabled = enable;
            btnQ.IsEnabled = enable;
            btnR.IsEnabled = enable;
            btnS.IsEnabled = enable;
            btnT.IsEnabled = enable;
            btnU.IsEnabled = enable;
            btnV.IsEnabled = enable;
            btnW.IsEnabled = enable;
            btnX.IsEnabled = enable;
            btnY.IsEnabled = enable;
            btnZ.IsEnabled = enable;
            btnSpace.IsEnabled = enable;
            btnApostrophy.IsEnabled = enable;
            btnBackSlash.IsEnabled = enable;
            btnFrontSlash.IsEnabled = enable;
            btnHyphen.IsEnabled = enable;
        }

        private void BtnA_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "A";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "A";
                    break;
            }
            UpdateInfo();
        }

        private void BtnB_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "B";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "B";
                    break;
            }
            UpdateInfo();
        }

        private void BtnC_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "C";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "C";
                    break;
            }
            UpdateInfo();
        }

        private void BtnD_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "D";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "D";
                    break;

            }
            UpdateInfo();
        }

        private void BtnE_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "E";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "E";
                    break;
            }
            UpdateInfo();
        }

        private void BtnF_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "F";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "F";
                    break;
            }
            UpdateInfo();
        }

        private void BtnG_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "G";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "G";
                    break;
            }
            UpdateInfo();
        }

        private void BtnH_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "H";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "H";
                    break;
            }
            UpdateInfo();
        }

        private void BtnI_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "I";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "I";
                    break;
            }
            UpdateInfo();
        }

        private void BtnJ_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "J";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "J";
                    break;
            }
            UpdateInfo();
        }

        private void BtnK_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "K";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "K";
                    break;
            }
            UpdateInfo();
        }

        private void BtnL_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "L";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "L";
                    break;
            }
            UpdateInfo();
        }

        private void BtnM_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "M";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "M";
                    break;
            }
            UpdateInfo();
        }

        private void BtnN_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "N";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "N";
                    break;
            }
            UpdateInfo();
        }

        private void BtnO_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "O";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "O";
                    break;
            }
            UpdateInfo();
        }

        private void BtnP_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "P";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "P";
                    break;
            }
            UpdateInfo();
        }

        private void BtnQ_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "Q";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "Q";
                    break;
            }
            UpdateInfo();
        }

        private void BtnR_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "R";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "R";
                    break;
            }
            UpdateInfo();
        }

        private void BtnS_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "S";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "S";
                    break;
            }
            UpdateInfo();
        }

        private void BtnT_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "T";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "T";
                    break;
            }
            UpdateInfo();
        }

        private void BtnU_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "U";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "U";
                    break;
            }
            UpdateInfo();
        }

        private void BtnV_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "V";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "V";
                    break;
            }
            UpdateInfo();
        }

        private void BtnW_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "W";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "W";
                    break;
            }
            UpdateInfo();
        }

        private void BtnX_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "X";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "X";
                    break;
            }
            UpdateInfo();
        }

        private void BtnY_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "Y";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "Y";
                    break;
            }
            UpdateInfo();
        }

        private void BtnZ_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "Z";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "Z";
                    break;
            }
            UpdateInfo();
        }

        private void BtnFrontSlash_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "\\";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "\\";
                    break;
            }
            UpdateInfo();
        }

        private void BtnHyphen_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "-";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "-";
                    break;
            }
            UpdateInfo();
        }

        private void BtnApostrophy_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "'";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "'";
                    break;
            }
            UpdateInfo();
        }

        private void BtnBackSlash_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + "/";
                    break;
                case 1:
                    yourNameInput.Text = YourName + "/";
                    break;
            }
            UpdateInfo();
        }

        private void Btn0_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 1:
                    yourNameInput.Text = YourName + "0";
                    break;

                case 2:
                    phoneNumberInput.Text = PhoneNumber + "0";
                    break;

                case 3:
                    idNumberInput.Text = IdNumber + "0";
                    break;

                case 0:
                    narrationInput.Text = Narration + "0";
                    break;
            }
            UpdateInfo();
        }

        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 1:
                    yourNameInput.Text = YourName + "1";
                    break;

                case 2:
                    phoneNumberInput.Text = PhoneNumber + "1";
                    break;

                case 3:
                    idNumberInput.Text = IdNumber + "1";
                    break;

                case 0:
                    narrationInput.Text = Narration + "1";
                    break;
            }
            UpdateInfo();
        }

        private void Btn2_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 1:
                    yourNameInput.Text = YourName + "2";
                    break;

                case 2:
                    phoneNumberInput.Text = PhoneNumber + "2";
                    break;

                case 3:
                    idNumberInput.Text = IdNumber + "2";
                    break;

                case 0:
                    narrationInput.Text = Narration + "2";
                    break;
            }
            UpdateInfo();
        }

        private void Btn3_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 1:
                    yourNameInput.Text = YourName + "3";
                    break;

                case 2:
                    phoneNumberInput.Text = PhoneNumber + "3";
                    break;

                case 3:
                    idNumberInput.Text = IdNumber + "3";
                    break;

                case 0:
                    narrationInput.Text = Narration + "3";
                    break;
            }
            UpdateInfo();
        }

        private void Btn4_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 1:
                    yourNameInput.Text = YourName + "4";
                    break;

                case 2:
                    phoneNumberInput.Text = PhoneNumber + "4";
                    break;

                case 3:
                    idNumberInput.Text = IdNumber + "4";
                    break;

                case 0:
                    narrationInput.Text = Narration + "4";
                    break;
            }
            UpdateInfo();
        }

        private void Btn5_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 1:
                    yourNameInput.Text = YourName + "5";
                    break;

                case 2:
                    phoneNumberInput.Text = PhoneNumber + "5";
                    break;

                case 3:
                    idNumberInput.Text = IdNumber + "5";
                    break;

                case 0:
                    narrationInput.Text = Narration + "5";
                    break;
            }
            UpdateInfo();
        }

        private void Btn6_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 1:
                    yourNameInput.Text = YourName + "6";
                    break;

                case 2:
                    phoneNumberInput.Text = PhoneNumber + "6";
                    break;

                case 3:
                    idNumberInput.Text = IdNumber + "6";
                    break;

                case 0:
                    narrationInput.Text = Narration + "6";
                    break;
            }
            UpdateInfo();
        }

        private void Btn7_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 1:
                    yourNameInput.Text = YourName + "7";
                    break;

                case 2:
                    phoneNumberInput.Text = PhoneNumber + "7";
                    break;

                case 3:
                    idNumberInput.Text = IdNumber + "7";
                    break;

                case 0:
                    narrationInput.Text = Narration + "7";
                    break;
            }
            UpdateInfo();
        }

        private void Btn8_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 1:
                    yourNameInput.Text = YourName + "8";
                    break;

                case 2:
                    phoneNumberInput.Text = PhoneNumber + "8";
                    break;

                case 3:
                    idNumberInput.Text = IdNumber + "8";
                    break;

                case 0:
                    narrationInput.Text = Narration + "8";
                    break;
            }
            UpdateInfo();
        }

        private void Btn9_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 1:
                    yourNameInput.Text = YourName + "9";
                    break;

                case 2:
                    phoneNumberInput.Text = PhoneNumber + "9";
                    break;

                case 3:
                    idNumberInput.Text = IdNumber + "9";
                    break;

                case 0:
                    narrationInput.Text = Narration + "9";
                    break;
            }
            UpdateInfo();
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 1:
                    if (YourName.Length != 0)
                        yourNameInput.Text = YourName.Remove(YourName.Length - 1, 1);
                    break;

                case 2:
                    if (PhoneNumber.Length != 0)
                        phoneNumberInput.Text = PhoneNumber.Remove(PhoneNumber.Length - 1, 1);
                    break;

                case 3:
                    if (IdNumber.Length != 0)
                        idNumberInput.Text = IdNumber.Remove(IdNumber.Length - 1, 1);
                    break;

                case 0:
                    if (Narration.Length != 0)
                        narrationInput.Text = Narration.Remove(Narration.Length - 1, 1);
                    break;
            }
            UpdateInfo();
        }

        private void BtnSpace_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationInput.Text = Narration + " ";
                    break;
                case 1:
                    yourNameInput.Text = YourName + " ";
                    break;
            }
            UpdateInfo();
        }

        private void UpdateInfo()
        {
            Narration = narrationInput.Text;
            YourName = yourNameInput.Text;
            PhoneNumber = phoneNumberInput.Text;
            IdNumber = idNumberInput.Text;
            mainView.narration = Narration;
            mainView.yourName = YourName;
            mainView.phoneNumber = PhoneNumber;
            mainView.idNumber = IdNumber;


            /*if (IdNumber.Length < 7)
            {
                btnNext.IsEnabled = false;
                mainView.btnRight.IsEnabled = false;
            }
            else
            {
                btnNext.IsEnabled = false;
                mainView.btnRight.IsEnabled = true;
            }*/
        }

        private void SetFieldFocus()
        {
            switch (FieldFocus)
            {
                case 0:
                    narrationBorder.BorderBrush = new SolidColorBrush(Colors.Blue);
                    yourNameBorder.BorderBrush = new SolidColorBrush(Colors.Black);
                    phoneNumberBorder.BorderBrush = new SolidColorBrush(Colors.Black);
                    idNumberBorder.BorderBrush = new SolidColorBrush(Colors.Black);
                    narrationInput.Focus();

                    EnableAlphabeticalKeys(true);
                    EnableNumericKeys(true);
                    break;

                case 1:
                    narrationBorder.BorderBrush = new SolidColorBrush(Colors.Black);
                    yourNameBorder.BorderBrush = new SolidColorBrush(Colors.Blue);
                    phoneNumberBorder.BorderBrush = new SolidColorBrush(Colors.Black);
                    idNumberBorder.BorderBrush = new SolidColorBrush(Colors.Black);
                    yourNameInput.Focus();

                    EnableAlphabeticalKeys(true);
                    EnableNumericKeys(true);
                    break;

                case 2:
                    narrationBorder.BorderBrush = new SolidColorBrush(Colors.Black);
                    yourNameBorder.BorderBrush = new SolidColorBrush(Colors.Black);
                    phoneNumberBorder.BorderBrush = new SolidColorBrush(Colors.Blue);
                    idNumberBorder.BorderBrush = new SolidColorBrush(Colors.Black);
                    phoneNumberInput.Focus();

                    EnableAlphabeticalKeys(false);
                    EnableNumericKeys(true);
                    break;

                case 3:
                    narrationBorder.BorderBrush = new SolidColorBrush(Colors.Black);
                    yourNameBorder.BorderBrush = new SolidColorBrush(Colors.Black);
                    phoneNumberBorder.BorderBrush = new SolidColorBrush(Colors.Black);
                    idNumberBorder.BorderBrush = new SolidColorBrush(Colors.Blue);
                    idNumberInput.Focus();

                    EnableAlphabeticalKeys(false);
                    EnableNumericKeys(true);
                    break;

            }
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            switch (FieldFocus)
            {
                case 0:
                    if (narrationInput.Text.Length < 5)
                    {
                        narrationLabel.Foreground = new SolidColorBrush(Colors.Red);
                        narrationLabel.Text = "Narration REQUIRED!!!";
                    }
                    else
                        FieldFocus = 1;
                    break;

                case 1:
                    if (yourNameInput.Text.Length < 5)
                    {
                        yourNameLabel.Foreground = new SolidColorBrush(Colors.Red);
                        yourNameLabel.Text = "Your Name REQUIRED!!!";
                    }
                    else
                        FieldFocus = 2;
                    break;

                case 2:
                    if (phoneNumberInput.Text.Length < 9)
                    {
                        phoneNumberLabel.Foreground = new SolidColorBrush(Colors.Red);
                        phoneNumberLabel.Text = "Phone Number REQUIRED!!!";
                    }
                    else
                        FieldFocus = 3;
                    break;

                case 3:
                    if (idNumberInput.Text.Length < 7)
                    {
                        idNumberLabel.Foreground = new SolidColorBrush(Colors.Red);
                        idNumberLabel.Text = "Id Number REQUIRED!!!";
                    }
                    else
                    {
                        FieldFocus = 3;
                        EnableNumericKeys(false);
                        btnNext.IsEnabled = false;
                        mainView.btnRight.IsEnabled = true;
                    }                        
                    break;
            }
            SetFieldFocus();
        }

        private void NarrationInput_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            FieldFocus = 0;
            SetFieldFocus();
            narrationInput.Focus();
            narrationBorder.BorderBrush = new SolidColorBrush(Colors.Blue);
        }

        private void YourNameInput_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            FieldFocus = 1;
            SetFieldFocus();
            yourNameInput.Focus();
            yourNameBorder.BorderBrush = new SolidColorBrush(Colors.Blue);
        }

        private void PhoneNumberInput_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            FieldFocus = 2;
            SetFieldFocus();
            phoneNumberInput.Focus();
            phoneNumberBorder.BorderBrush = new SolidColorBrush(Colors.Blue);
        }

        private void IdNumberInput_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            FieldFocus = 3;
            SetFieldFocus();
            idNumberInput.Focus();
            idNumberBorder.BorderBrush = new SolidColorBrush(Colors.Blue);
        }

        public void ManageView()
        {
            connects = connects + 1;
            Visibility = Visibility.Collapsed;
            mainView.homePage.Visibility = Visibility.Collapsed;
            mainView.disclaimerScreen.Visibility = Visibility.Collapsed;
            mainView.termsScreen.Visibility = Visibility.Collapsed;
            mainView.accountScreen.Visibility = Visibility.Collapsed;
            mainView.verifyScreen.Visibility = Visibility.Collapsed;
            mainView.countScreen.Visibility = Visibility.Collapsed;
            mainView.transactionScreen.Visibility = Visibility.Collapsed;
            mainView.finishScreen.Visibility = Visibility.Collapsed;
            mainView.ChangeRightButtonToDarkOrange(AppUtils.BtnContinue);
            mainView.btnRight.IsEnabled = true;
            SetupFields();

            mainView.btnLeft.Visibility = Visibility.Hidden;
            mainView.btnRight.Visibility = Visibility.Hidden;
            mainView.mcLoading.IsActive = true;

            AsyncStatusReport asyncStatusReport = new AsyncStatusReport(mainView.socket);
            asyncStatusReport.Completed += StatusCheckerCompleted;

            try
            {
                asyncStatusReport.StartAsync();
            }
            catch (Exception)
            {
                asyncStatusReport.Dispose();
            }
        }

        public void StatusCheckerCompleted(object sender, RunWorkerCompletedEventArgs args)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            worker.Dispose();

            StatusReport status = (StatusReport)args.Result;

            Console.WriteLine("Controller Status: " + status.ControllerState);
            if (status.ControllerState == ApiUtils.ControllerState.IDLE.ToString())
            {
                mainView.btnLeft.Visibility = Visibility.Visible;
                mainView.btnRight.Visibility = Visibility.Visible;
                mainView.mcLoading.IsActive = false;
                Visibility = Visibility.Visible;
            }
            else
            {
                if (connects < 3)
                {
                    Console.WriteLine("Rechecking Controller Status");
                    ManageView();
                }
            }
        }

    }
}
