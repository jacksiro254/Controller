﻿using ManiwaCDM.Data.Models;
using ManiwaCDM.Services;
using ManiwaCDM.Utils;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace ManiwaCDM.Views.Screens
{
    /// <summary>
    /// Interaction logic for CountScreen.xaml
    /// </summary>
    public partial class CountScreen : UserControl
    {
        readonly MainView mainView;
        int connects;

        public CountScreen(MainView _mainView)
        {
            InitializeComponent();
            mainView = _mainView;
        }

        public void SetupFields()
        {
            connects = connects + 1;
        }

        private void BtnCount_Click(object sender, RoutedEventArgs e)
        {
            mainView.DropRequest();
        }
        
        public void StartCount()
        {
            mainView.mcLoading.IsActive = true;
            mainView.tmrTimer1.Enabled = true;
            mainView.tmrTimer2.Interval = 100;
            mainView.tmrTimer2.Enabled = true;
        }

        public void DoCount()
        {
            mainView.count50 = mainView.count50 + 1;
            mainView.count100 = mainView.count100 + 1;
            mainView.count200 = mainView.count200 + 1;
            mainView.count500 = mainView.count500 + 1;
            mainView.count1000 = mainView.count1000 + 1;
        }

        public void DoNomalise()
        {
            mainView.count50 = mainView.count50 / 2;
            mainView.count100 = mainView.count100 / 3;
            mainView.count200 = mainView.count200 / 4;
            mainView.count500 = mainView.count500 / 5;
            mainView.count1000 = mainView.count1000 / 8;
        }

        public void ManageView()
        {
            connects = connects + 1;
            Visibility = Visibility.Collapsed;
            mainView.homePage.Visibility = Visibility.Collapsed;
            mainView.disclaimerScreen.Visibility = Visibility.Collapsed;
            mainView.termsScreen.Visibility = Visibility.Collapsed;
            mainView.accountScreen.Visibility = Visibility.Collapsed;
            mainView.narrationScreen.Visibility = Visibility.Collapsed;
            mainView.verifyScreen.Visibility = Visibility.Collapsed;
            mainView.transactionScreen.Visibility = Visibility.Collapsed;
            mainView.finishScreen.Visibility = Visibility.Collapsed;
            mainView.ChangeRightButtonToDarkOrange(AppUtils.BtnContinue);
            mainView.btnRight.IsEnabled = true;

            mainView.btnLeft.Visibility = Visibility.Hidden;
            mainView.btnRight.Visibility = Visibility.Hidden;
            mainView.mcLoading.IsActive = true;

            AsyncStatusReport asyncStatusReport = new AsyncStatusReport(mainView.socket);
            asyncStatusReport.Completed += StatusCheckerCompleted;

            try
            {
                asyncStatusReport.StartAsync();
            }
            catch (Exception)
            {
                asyncStatusReport.Dispose();
            }
        }

        public void StatusCheckerCompleted(object sender, RunWorkerCompletedEventArgs args)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            worker.Dispose();

            StatusReport status = (StatusReport)args.Result;

            Console.WriteLine("Controller Status: " + status.ControllerState);
            if (status.ControllerState == ApiUtils.ControllerState.IDLE.ToString()
            || status.ControllerState == ApiUtils.ControllerState.DROP.ToString())
            {
                mainView.btnLeft.Visibility = Visibility.Visible;
                mainView.btnRight.Visibility = Visibility.Hidden;
                mainView.mcLoading.IsActive = false;
                Visibility = Visibility.Visible;
            }
            else
            {
                if (connects < 3)
                {
                    Console.WriteLine("Rechecking Controller Status");
                    ManageView();
                }
            }
        }

    }
}
