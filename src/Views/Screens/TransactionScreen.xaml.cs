﻿using ManiwaCDM.Utils;
using System.Windows;
using System.Windows.Controls;

namespace ManiwaCDM.Views.Screens
{
    /// <summary>
    /// Interaction logic for ViewAlert.xaml
    /// </summary>
    public partial class TransactionScreen : UserControl
    {
        readonly MainView mainView;
        int connects;

        public TransactionScreen(MainView _mainView)
        {
            InitializeComponent();
            mainView = _mainView;
        }

        public void SetupFields()
        {
            connects = connects + 1;
            mainView.mcLoading.IsActive = false;
            mainView.tmrTimer1.Enabled = false;
            mainView.tmrTimer2.Enabled = false;
            mainView.currentScreen = 7;
            mainView.SetCurrentScreen();
            CheckCount();
        }

        public void CheckCount()
        {
            Count50 = mainView.count50;
            Count100 = mainView.count100;
            Count200 = mainView.count200;
            Count500 = mainView.count500;
            Count1000 = mainView.count1000;

            Amount50 = Count50 * 50;
            Amount100 = Count100 * 100;
            Amount200 = Count200 * 200;
            Amount500 = Count500 * 500;
            Amount1000 = Count1000 * 1000;
            AmountTotal = Amount50 + Amount100 + Amount200 + Amount500 + Amount1000;
        }

        public int AmountTotal
        {
            get { return int.Parse(amountTotal.Text); }
            set { amountTotal.Text = "Total Amount. " + value.ToString(); }
        }

        public int Amount50
        {
            get { return int.Parse(amount50.Text); }
            set { amount50.Text = value.ToString(); }
        }

        public int Amount100
        {
            get { return int.Parse(amount100.Text); }
            set { amount100.Text = value.ToString(); }
        }

        public int Amount200
        {
            get { return int.Parse(amount200.Text); }
            set { amount200.Text = value.ToString(); }
        }

        public int Amount500
        {
            get { return int.Parse(amount500.Text); }
            set { amount500.Text = value.ToString(); }
        }

        public int Amount1000
        {
            get { return int.Parse(amount1000.Text); }
            set { amount1000.Text = value.ToString(); }
        }

        public int Count50
        {
            get { return int.Parse(count50.Text); }
            set { count50.Text = value.ToString(); }
        }

        public int Count100
        {
            get { return int.Parse(count100.Text); }
            set { count100.Text = value.ToString(); }
        }

        public int Count200
        {
            get { return int.Parse(count200.Text); }
            set { count200.Text = value.ToString(); }
        }

        public int Count500
        {
            get { return int.Parse(count500.Text); }
            set { count500.Text = value.ToString(); }
        }

        public int Count1000
        {
            get { return int.Parse(count1000.Text); }
            set { count1000.Text = value.ToString(); }
        }

        public void ShowBagActions(bool show)
        {
            if (show)
            {
                instructions.Text = AppUtils.BagInstructions;
                bagActions.Visibility = Visibility.Visible;
                moneyActions.Visibility = Visibility.Collapsed;
            }
            else
            {
                instructions.Text = AppUtils.MoneyInstructions;
                bagActions.Visibility = Visibility.Collapsed;
                moneyActions.Visibility = Visibility.Visible;
            }
        }

        private void BtnReject_Click(object sender, RoutedEventArgs e)
        {
            mainView.BagReject();
        }

        private void BtnDrop_Click(object sender, RoutedEventArgs e)
        {
            mainView.BagDrop();
        }

        private void BtnAddMore_Click(object sender, RoutedEventArgs e)
        {
            mainView.countScreen.StartCount();
        }

        private void BtnProceed_Click(object sender, RoutedEventArgs e)
        {
            ShowBagActions(true);
        }

        public void ManageView()
        {
            connects = connects + 1;
            Visibility = Visibility.Collapsed;
            mainView.disclaimerScreen.Visibility = Visibility.Collapsed;
            mainView.termsScreen.Visibility = Visibility.Collapsed;
            mainView.accountScreen.Visibility = Visibility.Collapsed;
            mainView.narrationScreen.Visibility = Visibility.Collapsed;
            mainView.verifyScreen.Visibility = Visibility.Collapsed;
            mainView.countScreen.Visibility = Visibility.Collapsed;
            mainView.transactionScreen.Visibility = Visibility.Collapsed;
            mainView.finishScreen.Visibility = Visibility.Collapsed;
            mainView.ChangeRightButtonToDarkOrange(AppUtils.BtnContinue);
            mainView.btnRight.IsEnabled = true;

            mainView.btnLeft.Visibility = Visibility.Visible;
            mainView.btnRight.Visibility = Visibility.Visible;
            mainView.mcLoading.IsActive = false;
            Visibility = Visibility.Visible;
        }

    }
}
