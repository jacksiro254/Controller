﻿using ManiwaCDM.Utils;
using System.Windows;
using System.Windows.Controls;

namespace ManiwaCDM.Views.Screens
{
    /// <summary>
    /// Interaction logic for VerifyScreen.xaml
    /// </summary>
    public partial class VerifyScreen : UserControl
    {
        readonly MainView mainView;

        public VerifyScreen(MainView _mainView)
        {
            InitializeComponent();
            mainView = _mainView;
            SetupFields();
        }

        public void SetupFields()
        {
            AccountNumber = mainView.accountNumber;
            AccountName = mainView.accountName;
            Narration = mainView.narration;
            YourName = mainView.yourName;
            PhoneNumber = mainView.phoneNumber;
            IdNumber = mainView.idNumber;
        }

        public string AccountNumber
        {
            get { return accNumberValue.Text; }
            set { accNumberValue.Text = value; }
        }

        public string AccountName
        {
            get { return accNameValue.Text; }
            set { accNameValue.Text = value; }
        }

        public string Narration
        {
            get { return narrationValue.Text; }
            set { narrationValue.Text = value; }
        }

        public string YourName
        {
            get { return yourNameValue.Text; }
            set { yourNameValue.Text = value; }
        }

        public string PhoneNumber
        {
            get { return phoneNumberValue.Text; }
            set { phoneNumberValue.Text = value; }
        }

        public string IdNumber
        {
            get { return idNumberValue.Text; }
            set { idNumberValue.Text = value; }
        }

        public void ManageView()
        {
            Visibility = Visibility.Collapsed;
            mainView.disclaimerScreen.Visibility = Visibility.Collapsed;
            mainView.termsScreen.Visibility = Visibility.Collapsed;
            mainView.accountScreen.Visibility = Visibility.Collapsed;
            mainView.narrationScreen.Visibility = Visibility.Collapsed;
            mainView.verifyScreen.Visibility = Visibility.Collapsed;
            mainView.countScreen.Visibility = Visibility.Collapsed;
            mainView.transactionScreen.Visibility = Visibility.Collapsed;
            mainView.finishScreen.Visibility = Visibility.Collapsed;
            mainView.ChangeRightButtonToDarkOrange(AppUtils.BtnContinue);
            mainView.btnRight.IsEnabled = true;

            mainView.btnLeft.Visibility = Visibility.Hidden;
            mainView.btnRight.Visibility = Visibility.Hidden;
            mainView.mcLoading.IsActive = true;
        }
    }
}
