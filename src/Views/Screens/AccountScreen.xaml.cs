﻿using ManiwaCDM.Controls;
using ManiwaCDM.Data.Models;
using ManiwaCDM.Services;
using ManiwaCDM.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace ManiwaCDM.Views.Screens
{
    /// <summary>
    /// Interaction logic for AccountScreen.xaml
    /// </summary>
    public partial class AccountScreen : UserControl
    {
        public List<McGridItem> mcGridItems;
        readonly MainView mainView;
        int connects;

        public AccountScreen(MainView _mainView)
        {
            InitializeComponent();
            mainView = _mainView;
            SetupFields();
        }

        public void SetupFields()
        {
            connects = connects + 1;
            Input = accountInput.Text = "";
            accountNumber.Text = Input;
            mainView.accountNumber = Input;
            ActivateInput();
        }


        public string Input
        {
            get { return accountInput.Text; }
            set { accountInput.Text = value; }
        }

        public string Title
        {
            get { return accountName.Text; }
            set { accountName.Text = value; }
        }

        public string Number
        {
            get { return accountNumber.Text; }
            set { accountNumber.Text = value; }
        }

        private void BtnZero_Click(object sender, RoutedEventArgs e)
        {
            accountInput.Text = Input + "0";
            UpdateInfo();
        }

        private void BtnOne_Click(object sender, RoutedEventArgs e)
        {
            accountInput.Text = Input + "1";
            UpdateInfo();
        }

        private void BtnTwo_Click(object sender, RoutedEventArgs e)
        {
            accountInput.Text = Input + "2";
            UpdateInfo();
        }

        private void BtnThree_Click(object sender, RoutedEventArgs e)
        {
            accountInput.Text = Input + "3";
            UpdateInfo();
        }

        private void BtnFour_Click(object sender, RoutedEventArgs e)
        {
            accountInput.Text = Input + "4";
            UpdateInfo();
        }

        private void BtnFive_Click(object sender, RoutedEventArgs e)
        {
            accountInput.Text = Input + "5";
            UpdateInfo();
        }

        private void BtnSix_Click(object sender, RoutedEventArgs e)
        {
            accountInput.Text = Input + "6";
            UpdateInfo();
        }

        private void BtnSeven_Click(object sender, RoutedEventArgs e)
        {
            accountInput.Text = Input + "7";
            UpdateInfo();
        }

        private void BtnEight_Click(object sender, RoutedEventArgs e)
        {
            accountInput.Text = Input + "8";
            UpdateInfo();
        }

        private void BtnNine_Click(object sender, RoutedEventArgs e)
        {
            accountInput.Text = Input + "9";
            UpdateInfo();
        }

        private void BtnBackspace_Click(object sender, RoutedEventArgs e)
        {
            if (Input.Length != 0)
            {
                accountInput.Text = Input.Remove(Input.Length - 1, 1);
                UpdateInfo();
            }
        }

        private void UpdateInfo()
        {
            Input = accountInput.Text;
            accountNumber.Text = Input;
            mainView.accountNumber = Input;
            if (Input.Length != 0)
            {
                accountName.Text = "Demo User";
                mainView.accountName = accountName.Text;
                mainView.btnRight.IsEnabled = true;
            }
            else
                mainView.btnRight.IsEnabled = false;
        }

        private void AccountInput_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ActivateInput();
        }

        private void ActivateInput()
        {
            accountInput.Focus();
            accountBorder.BorderBrush = new SolidColorBrush(Colors.Blue);
        }

        public void ManageView()
        {
            connects = connects + 1;
            Visibility = Visibility.Collapsed;
            mainView.homePage.Visibility = Visibility.Collapsed;
            mainView.disclaimerScreen.Visibility = Visibility.Collapsed;
            mainView.termsScreen.Visibility = Visibility.Collapsed;
            mainView.narrationScreen.Visibility = Visibility.Collapsed;
            mainView.verifyScreen.Visibility = Visibility.Collapsed;
            mainView.countScreen.Visibility = Visibility.Collapsed;
            mainView.transactionScreen.Visibility = Visibility.Collapsed;
            mainView.finishScreen.Visibility = Visibility.Collapsed;
            mainView.ChangeRightButtonToDarkOrange(AppUtils.BtnContinue);
            mainView.btnRight.IsEnabled = true;

            mainView.btnLeft.Visibility = Visibility.Hidden;
            mainView.btnRight.Visibility = Visibility.Hidden;
            mainView.mcLoading.IsActive = true;

            AsyncStatusReport asyncStatusReport = new AsyncStatusReport(mainView.socket);
            asyncStatusReport.Completed += StatusCheckerCompleted;

            try
            {
                asyncStatusReport.StartAsync();
            }
            catch (Exception)
            {
                asyncStatusReport.Dispose();
            }
        }

        public void StatusCheckerCompleted(object sender, RunWorkerCompletedEventArgs args)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            worker.Dispose();

            StatusReport status = (StatusReport)args.Result;

            Console.WriteLine("Controller Status: " + status.ControllerState);
            if (status.ControllerState == ApiUtils.ControllerState.IDLE.ToString())
            {
                mainView.btnLeft.Visibility = Visibility.Visible;
                mainView.btnRight.Visibility = Visibility.Visible;
                mainView.mcLoading.IsActive = false;
                Visibility = Visibility.Visible;
            }
            else
            {
                if (connects < 3)
                {
                    Console.WriteLine("Rechecking Controller Status");
                    ManageView();
                }
            }
        }

    }
}
