﻿using ManiwaCDM.Controls;
using ManiwaCDM.Data.Models;
using ManiwaCDM.Services;
using ManiwaCDM.Utils;
using ManiwaCDM.Views.Screens;
using System;
using System.ComponentModel;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;

namespace ManiwaCDM.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public int currentScreen, count, finishingCount;
        public string accountNumber, accountName, narration, yourName, phoneNumber, idNumber, feedback;
        public int count50, count100, count200, count500, count1000, amount50, amount100, amount200, amount500, amount1000, amountTotal;

        public McGridItem selected;

        public HomeScreen homePage;
        public DisclaimerScreen disclaimerScreen;
        public TermsScreen termsScreen;
        public AccountScreen accountScreen;
        public NarrationScreen narrationScreen;
        public CountScreen countScreen;
        public VerifyScreen verifyScreen;
        public TransactionScreen transactionScreen;
        public FinishScreen finishScreen;

        public Timer tmrTimer1, tmrTimer2, tmrSocket;

        public Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public MainView()
        {
            InitializeComponent();

            Height = 700;
            Width = 1000;
            //WindowState = WindowState.Maximized;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;

            count = 0;
            finishingCount = 4;
            count50 = count100 = count200 = count500 = count1000 = amount50 = amount100 = amount200 = amount500 = amount1000 = amountTotal = 0;
            feedback = "";
            currentScreen = 5;

            //Connect to the socket which will be used in the entire app
            socket.Connect(ApiUtils.iPEndPoint());
            Console.WriteLine("Socket connected to {0}", socket.RemoteEndPoint.ToString());

            //Send Authorise Request to the socket
            socket.Send(ApiUtils.AuthoriseBytes, 0, ApiUtils.AuthoriseBytes.Length, 0);

            //Generate a timer to manage the socket keep alive function
            tmrSocket = new Timer
            {
                Interval = 3000,
                Enabled = true
            };

            //Use the Timer Tick event for managing the socket
            tmrSocket.Tick += ((sender, args) => 
            {
                if (socket.Connected)
                {
                    //Send Acknowledgement Request to the socket
                    socket.Send(ApiUtils.AckBytes, 0, ApiUtils.AckBytes.Length, 0);
                }
            });

            Loaded += MainView_Loaded;
        }

        private void MainView_Loaded(object sender, RoutedEventArgs e)
        {            
            tmrTimer1 = new Timer 
            { 
                Interval = 3000,
                Enabled = false
            };
            tmrTimer1.Tick += TmrTiming;

            tmrTimer2 = new Timer
            {
                Interval = 1000,
                Enabled = false
            };
            tmrTimer2.Tick += TmrTiming1;

            homePage = new HomeScreen(this);
            disclaimerScreen = new DisclaimerScreen(this);
            termsScreen = new TermsScreen(this);
            accountScreen = new AccountScreen(this);
            narrationScreen = new NarrationScreen(this);
            verifyScreen = new VerifyScreen(this);
            countScreen = new CountScreen(this);
            transactionScreen = new TransactionScreen(this);
            finishScreen = new FinishScreen(this);

            Grid.SetRow(homePage, 1);
            Grid.SetRow(disclaimerScreen, 1);
            Grid.SetRow(termsScreen, 1);
            Grid.SetRow(accountScreen, 1);
            Grid.SetRow(narrationScreen, 1);
            Grid.SetRow(verifyScreen, 1);
            Grid.SetRow(countScreen, 1);
            Grid.SetRow(transactionScreen, 1);
            Grid.SetRow(finishScreen, 1);

            innerLayout.Children.Add(homePage);
            innerLayout.Children.Add(disclaimerScreen);
            innerLayout.Children.Add(termsScreen);
            innerLayout.Children.Add(accountScreen);
            innerLayout.Children.Add(narrationScreen);
            innerLayout.Children.Add(verifyScreen);
            innerLayout.Children.Add(countScreen);
            innerLayout.Children.Add(transactionScreen);
            innerLayout.Children.Add(finishScreen);

            homePage.LoadTransactionTypes();
            SetCurrentScreen();
        }

        public void DropRequest()
        {
            DropRequest dropRequest = new DropRequest();
            dropRequest.Currency = "KES";
            dropRequest.TransactionValue = count.ToString();
            dropRequest.InputNumber = "AABBCC";
            dropRequest.InputSubNumber = "0";
            dropRequest.Reference = "0";
            dropRequest.ShiftReference = "0";
            dropRequest.UserID = "1234";
            dropRequest.DropType = "NOTES";

            mcLoading.IsActive = true;
            AsyncDropRequest asyncDropCash = new AsyncDropRequest(socket);
            asyncDropCash.ccpMessage = new CcpMessage("120", "DropRequest", "20");
            asyncDropCash.dropReq = dropRequest;

            asyncDropCash.Completed += DropRequestCompleted;
            try
            {
                asyncDropCash.StartAsync();
            }
            catch (Exception)
            {
                asyncDropCash.Dispose();
            }
        }

        public void DropRequestCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            worker.Dispose();
            DropStatus dropStatus = (DropStatus)e.Result;

            Console.WriteLine("DropStatus: " + dropStatus.Status);

            mcLoading.IsActive = false;

            if (dropStatus.Status == "DROPPING")
            {
                currentScreen = 6;
                SetCurrentScreen();

            }
        }

        // Determine which screen to be shown to the user
        public void SetCurrentScreen()
        {
            switch (currentScreen)
            {
                case 1:
                    disclaimerScreen.ManageView();
                    break;

                case 2:
                    termsScreen.ManageView();
                    break;

                case 3:
                    accountScreen.SetupFields();
                    accountScreen.ManageView();
                    break;

                case 4:
                    narrationScreen.SetupFields();
                    narrationScreen.ManageView();
                    break;

                case 5:
                    countScreen.SetupFields();
                    countScreen.ManageView();
                    break;

                case 6:
                    transactionScreen.SetupFields();
                    transactionScreen.ManageView();
                    break;

                case 7:
                    verifyScreen.SetupFields();
                    verifyScreen.ManageView();
                    break;

                case 8:
                    finishScreen.ManageView();
                    break;

                default:
                    homePage.SetupFields();
                    homePage.ManageView();
                    break;
            }
        }

        public void ChangeRightButtonToDodgerBlue()
        {
            btnLeft.Content = AppUtils.BtnHelp;
            btnRight.Content = AppUtils.BtnMore;
            btnRight.Background = new SolidColorBrush(Colors.DodgerBlue);
            btnRight.BorderBrush = new SolidColorBrush(Colors.DodgerBlue);
        }

        public void ChangeRightButtonToDarkOrange(string btnRightText)
        {
            btnLeft.Content = AppUtils.BtnBack;
            btnRight.Content = btnRightText;
            btnRight.Background = new SolidColorBrush(Colors.DarkOrange);
            btnRight.BorderBrush = new SolidColorBrush(Colors.DarkOrange);
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        private void BtnLeft_Click(object sender, RoutedEventArgs e)
        {
            switch (currentScreen)
            {
                case 1:
                    currentScreen = 0;
                    SetCurrentScreen();
                    break;

                case 2:
                    currentScreen = 1;
                    SetCurrentScreen();
                    break;

                case 3:
                    currentScreen = 2;
                    SetCurrentScreen();
                    break;

                case 4:
                    currentScreen = 3;
                    SetCurrentScreen();
                    break;

                case 5:
                    currentScreen = 4;
                    SetCurrentScreen();
                    break;

                case 6:
                    currentScreen = 5;
                    SetCurrentScreen();
                    break;

                case 7:
                    currentScreen = 0;
                    SetCurrentScreen();
                    break;

                default:
                    currentScreen = 0;
                    SetCurrentScreen();
                    break;
            }
        }

        private void BtnRight_Click(object sender, RoutedEventArgs e)
        {
            switch (currentScreen)
            {
                case 1:
                    currentScreen = 2;
                    SetCurrentScreen();
                    break;

                case 2:
                    currentScreen = 3;
                    SetCurrentScreen();
                    break;

                case 3:
                    currentScreen = 4;
                    SetCurrentScreen();
                    break;

                case 4:
                    currentScreen = 5;
                    SetCurrentScreen();
                    break;

                case 5:
                    currentScreen = 6;
                    SetCurrentScreen();
                    break;

                case 6:
                    currentScreen = 7;
                    SetCurrentScreen();
                    break;

                case 7:
                    currentScreen = 8;
                    SetCurrentScreen();
                    break;

                default:
                    currentScreen = 0;
                    SetCurrentScreen();
                    break;
            }
        }

        private void TmrTiming(object sender, EventArgs e)
        {
            countScreen.DoNomalise();
            transactionScreen.SetupFields();
        }

        private void TmrTiming1(object sender, EventArgs e)
        {
            switch (currentScreen)
            {
                case 6:
                case 7:
                    countScreen.DoCount();
                    break;

                case 8:
                    finishingCount = finishingCount - 1;
                    finishScreen.Text = "This Screen will close in " + finishingCount.ToString() + " sec";
                    currentScreen = 0;
                    SetCurrentScreen();
                    break;
            }
        }

        public void BagScreen()
        {
            mcLoading.IsActive = true;
            transactionScreen.btnDrop.IsEnabled = false;
            transactionScreen.btnReject.IsEnabled = false;
            tmrTimer1.Enabled = true;
            tmrTimer2.Interval = 1;
            tmrTimer2.Enabled = true;
        }

        public void FinishScreen()
        {
            tmrTimer1.Enabled = true;
            tmrTimer2.Interval = 1000;
            tmrTimer2.Enabled = true;
        }

        public void BagReject()
        {
            currentScreen = 0;
            SetCurrentScreen();
        }

        public void BagDrop()
        {
            mcLoading.IsActive = true;
            transactionScreen.btnDrop.IsEnabled = false;
            transactionScreen.btnReject.IsEnabled = false;

        }

    }
}
