﻿namespace ManiwaCDM.Data.Models
{
    public class Cassette
	{
		public string No { get; set; }
		public string Denom { get; set; }
		public string Level { get; set; }
		public string Capacity { get; set; }
		public string Status { get; set; }
		public string Type { get; set; }

		public Cassette() { }

		public Cassette(
			string _no,
			string _denom,
			string _level,
			string _capacity,
			string _status,
			string _type
		)
		{
			No = _no;
			Denom = _denom;
			Level = _level;
			Capacity = _capacity;
			Status = _status;
			Type = _type;
		}
	}
}
