﻿namespace ManiwaCDM.Data.Models
{

	public class AuthoriseResponse
	{
		public string ClientID { get; set; }
		public string Result { get; set; }
		public string FailCode { get; set; }
		public string SoftwareVersion { get; set; }

		public AuthoriseResponse() { }

		public AuthoriseResponse(
			string _client_id,
			string _result,
			string _fail_code,
			string _software_version
		)
		{
			ClientID = _client_id;
			Result = _result;
			FailCode = _fail_code;
			SoftwareVersion = _software_version;
		}
	}

}
