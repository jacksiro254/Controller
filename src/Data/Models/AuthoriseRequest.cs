﻿namespace ManiwaCDM.Data.Models
{

	public class AuthoriseRequest
	{
		public string ClientID { get; set; }

		public string MACAddress { get; set; }

		public string ClientType { get; set; }

		public AuthoriseRequest() { }

		public AuthoriseRequest(
			string _client_id, 
			string _mac_address, 
			string _client_type
		)
		{
			ClientID = _client_id;
			MACAddress = _mac_address;
			ClientType = _client_type;
		}
	}

}
