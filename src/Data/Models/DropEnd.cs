﻿namespace ManiwaCDM.Data.Models
{

	public class DropEnd
	{
		public string Message { get; set; }
		public string Escrow { get; set; }

		public DropEnd() { }

		public DropEnd(
			string _message,
			string _escrow
		)
		{
			Message = _message;
			Escrow = _escrow;
		}
	}

}
