﻿namespace ManiwaCDM.Data.Models
{
    public class BA
	{
		public string Type { get; set; }
		public string Status { get; set; }
		public string Currency { get; set; }

		public BA() { }

		public BA(
			string _type,
			string _status,
			string _currency
		)
		{
			Type = _type;
			Status = _status;
			Currency = _currency;
		}
	}
}
