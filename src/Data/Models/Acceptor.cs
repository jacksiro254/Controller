﻿using System.Collections.Generic;

namespace ManiwaCDM.Data.Models
{
    public class Acceptor
	{
		public BA BA { get; set; }
		public BA BA1 { get; set; }
		public CA CA { get; set; }

		public Acceptor() { }

		public Acceptor(
			BA _ba,
			BA _ba1,
			CA _ca
		)
		{
			BA = _ba;
			BA1 = _ba1;
			CA = _ca;
		}
	}
}
