﻿namespace ManiwaCDM.Data.Models
{

	public class CcpMessage
	{
		public string MsgID { get; set; }

		public string MsgName { get; set; }

		public string SeqNo { get; set; }

		public CcpMessage() { }

		public CcpMessage(
			string _msg_id, 
			string _msg_name, 
			string _msg_number
		)
		{
			MsgID = _msg_id;
			MsgName = _msg_name;
			SeqNo = _msg_number;
		}
	}

}
