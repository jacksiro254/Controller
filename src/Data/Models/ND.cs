﻿using System.Collections.Generic;

namespace ManiwaCDM.Data.Models
{
    public class ND
	{
		public List<Cassette> Cassettes { get; set; }
		public string Type { get; set; }
		public string DeviceID { get; set; }
		public string Status { get; set; }
		public string CassetteState { get; set; }
		public string Currency { get; set; }
		public string TotalValue { get; set; }
		public string DispenseCurrency { get; set; }

		public ND() { }

		public ND(
			List<Cassette> _cassette,
			string _type,
			string _device_id,
			string _status,
			string _cassette_state,
			string _currency,
			string _total_value,
			string _dispense_currency
		)
		{
			Cassettes = _cassette;
			Type = _type;
			DeviceID = _device_id;
			Status = _status;
			CassetteState = _cassette_state;
			Currency = _currency;
			TotalValue = _total_value;
			DispenseCurrency = _dispense_currency;
		}
	}
}
