﻿namespace ManiwaCDM.Data.Models
{
    public class CardReader
	{
		public string Type { get; set; }
		public string Status { get; set; }
		public string ErrorString { get; set; }
		public string Card { get; set; }

		public CardReader() { }

		public CardReader(
			string _type,
			string _status,
			string _error_string,
			string _card
		)
		{
			Type = _type;
			Status = _status;
			ErrorString = _error_string;
			Card = _card;
		}
	}
}
