﻿namespace ManiwaCDM.Data.Models
{

	public class DropRequest
	{
		public string Currency { get; set; }
		public string TransactionValue { get; set; }
		public string InputNumber { get; set; }
		public string InputSubNumber { get; set; }
		public string Reference { get; set; }
		public string ShiftReference { get; set; }
		public string UserID { get; set; }
		public string DropType { get; set; }

		public DropRequest() { }

		public DropRequest(
			string _currency,
			string _transaction_value,
			string _input_number,
			string _input_sub_number,
			string _reference,
			string _shift_reference,
			string _userid,
			string _drop_type
		)
		{
			Currency = _currency;
			TransactionValue = _transaction_value;
			InputNumber = _input_number;
			InputSubNumber = _input_sub_number;
			Reference = _reference;
			ShiftReference = _shift_reference;
			UserID = _userid;
			DropType = _drop_type;
		}
	}

}
