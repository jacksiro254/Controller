﻿using System.Collections.Generic;

namespace ManiwaCDM.Data.Models
{
    public class Bag
    {
		public string Number { get; set; }
		public string Status { get; set; }
		public string NoteLevel { get; set; }
		public string NoteCapacity { get; set; }
		public string ValueLevel { get; set; }
		public string ValueCapacity { get; set; }
		public string PercentFull { get; set; }
		public List<NoteCount> NoteCounts { get; set; }

		public Bag() { }

		public Bag(
			string _number,
			string _status,
			string _note_level,
			string _note_capacity,
			string _value_level,
			string _value_capacity,
			string _percent_full,
			List<NoteCount> _note_counts
		)
		{
			Number = _number;
			Status = _status;
			NoteLevel = _note_level;
			NoteCapacity = _note_capacity;
			ValueLevel = _value_level;
			ValueCapacity = _value_capacity;
			PercentFull = _percent_full;
			NoteCounts = _note_counts;
		}
	}
}
