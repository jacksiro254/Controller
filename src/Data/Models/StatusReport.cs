﻿namespace ManiwaCDM.Data.Models
{
	public class StatusReport
	{
        public string ControllerState { get; set; }
		public Acceptor Acceptors { get; set; }
		public Dispenser Dispensers { get; set; }
		public Bag Bag { get; set; }
		public Sensor Sensors { get; set; }
		public Escrow Escrow { get; set; }
		public Transaction Transaction { get; set; }
		public CardReader CardReader { get; set; }
		public string DateTime { get; set; }

		public StatusReport() { }

		public StatusReport(
			string _controller_state,
			Acceptor _acceptors,
			Dispenser _dispensers,
			Bag _bag,
			Sensor _sensors,
			Escrow _escrow,
			Transaction _transaction,
			CardReader _card_reader,
			string _date_time
		)
		{
			ControllerState = _controller_state;
			Acceptors = _acceptors;
			Dispensers = _dispensers;
			Bag = _bag;
			Sensors = _sensors;
			Escrow = _escrow;
			Transaction = _transaction;
			CardReader = _card_reader;
			DateTime = _date_time;
		}
	}

}
