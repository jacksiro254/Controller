﻿namespace ManiwaCDM.Data.Models
{

	public class DropStatus
	{
		public string Status { get; set; }
		public string TransactionValue { get; set; }
		public string TotalValue { get; set; }
		public string TotalCount { get; set; }
		public NoteCount NoteCounts { get; set; }
		public string OutstandingBalance { get; set; }

		public DropStatus() { }

		public DropStatus(
			string _status,
			string _total_value,
			string _total_count,
			NoteCount _note_counts,
			string _outstanding_balance
		)
		{
			Status = _status;
			TotalValue = _total_value;
			TotalCount = _total_count;
			NoteCounts = _note_counts;
			OutstandingBalance = _outstanding_balance;
		}
	}

}
