﻿namespace ManiwaCDM.Data.Models
{

	public class DropPause
	{
		public string Message { get; set; }

		public DropPause() { }

		public DropPause(
			string _message
		)
		{
			Message = _message;
		}
	}

}
