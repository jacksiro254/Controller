﻿namespace ManiwaCDM.Data.Models
{

	public class NoteCount
	{
		public string Currency { get; set; }
		public string Denomination { get; set; }
		public string Count { get; set; }

		public NoteCount() { }

		public NoteCount(
			string _currency, 
			string _denomination, 
			string _count
		)
		{
			Currency = _currency;
			Denomination = _denomination;
			Count = _count;
		}
	}

}
