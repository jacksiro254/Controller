﻿namespace ManiwaCDM.Data.Models
{
    public class Sensor
	{
		public string Type { get; set; }
		public string Status { get; set; }
		public string Value { get; set; }
		public string Door { get; set; }
		public string AuxDoor { get; set; }
		public string Bag { get; set; }

		public Sensor() { }

		public Sensor(
			string _type,
			string _status,
			string _value,
			string _door,
			string _aux_door,
			string _bag
		)
		{
			Type = _type;
			Status = _status;
			Value = _value;
			Door = _door;
			AuxDoor = _aux_door;
			Bag = _bag;
		}
	}
}
