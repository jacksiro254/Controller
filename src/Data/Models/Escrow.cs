﻿namespace ManiwaCDM.Data.Models
{
    public class Escrow
    {
		public string Type { get; set; }
		public string Status { get; set; }
		public string Position { get; set; }

		public Escrow() { }

		public Escrow(
			string _type,
			string _status,
			string _position
		)
		{
			Type = _type;
			Status = _status;
			Position = _position;
		}
	}
}
