﻿using System.Collections.Generic;

namespace ManiwaCDM.Data.Models
{
    public class Transaction
	{
		public string Status { get; set; }
		public string Type { get; set; }

		public Transaction() { }

		public Transaction(
			string _status,
			string _type
		)
		{
			Status = _status;
			Type = _type;
		}
	}
}
