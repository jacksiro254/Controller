﻿namespace ManiwaCDM.Data.Models
{

	public class DropResult
	{
		public string SoftwareVersion { get; set; }
		public string DeviceProductName { get; set; }
		public string DeviceSerialNumber { get; set; }
		public string LocationName { get; set; }
		public string InputNumber { get; set; }
		public string InputSubNumber { get; set; }
		public string OutputNumber { get; set; }
		public string OutputSubNumber { get; set; }
		public string TrxCode { get; set; }
		public string BagNumber { get; set; }
		public string Rejected { get; set; }
		public string NoteJam { get; set; }
		public string DropMode { get; set; }
		public string TranAmount { get; set; }
		public string TranCycle { get; set; }
		public string ContainerCycle { get; set; }
		public string ContainerNumber { get; set; }
		public string TotalNumberOfNotes { get; set; }
		public NoteCount NoteCounts { get; set; }
		public string Reference { get; set; }
		public string ShiftReference { get; set; }
		public string DateTime { get; set; }

		public DropResult() { }

		public DropResult(
			string _software_version,
			string _device_product_name,
			string _device_serial_number,
			string _location_name,
			string _input_number,
			string _input_sub_number,
			string _output_number,
			string _output_sub_number,
			string _trx_code,
			string _bag_number,
			string _rejected,
			string _note_jam,
			string _drop_mode,
			string _tran_amount,
			string _tran_cycle,
			string _container_cycle,
			string _container_number,
			string _total_number_of_notes,
			NoteCount _note_counts,
			string _reference,
			string _shift_reference,
			string _date_time
		)
		{
			SoftwareVersion = _software_version;
			DeviceProductName = _device_product_name;
			DeviceSerialNumber = _device_serial_number;
			LocationName = _location_name;
			InputNumber = _input_number;
			InputSubNumber = _input_sub_number;
			OutputNumber = _output_number;
			OutputSubNumber = _output_sub_number;
			TrxCode = _trx_code;
			BagNumber = _bag_number;
			Rejected = _rejected;
			NoteJam = _note_jam;
			DropMode = _drop_mode;
			TranAmount = _tran_amount;
			TranCycle = _tran_cycle;
			ContainerCycle = _container_cycle;
			ContainerNumber = _container_number;
			TotalNumberOfNotes = _total_number_of_notes;
			NoteCounts = _note_counts;
			Reference = _reference;
			ShiftReference = _shift_reference;
			DateTime = _date_time;
		}
	}

}
