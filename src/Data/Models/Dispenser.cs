﻿using System.Collections.Generic;

namespace ManiwaCDM.Data.Models
{
    public class Dispenser
	{
		public List<ND> NDs { get; set; }

		public Dispenser() { }

		public Dispenser(
			List<ND> _nd
		)
		{
			NDs = _nd;
		}
	}
}
