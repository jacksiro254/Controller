﻿using ManiwaCDM.Data.Models;
using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;

namespace ManiwaCDM.Utils
{
    class ApiUtils
    {
        public enum ControllerState
        {
            INIT,
            IDLE,
            DROP,
            DROP_PAUSED,
            ESCROW_DROP,
            ESCROW_REJECT,
            DISPENSE,
            OUT_OF_ORDER,
        }

        //Establish the remote endpoint for the socket at the controller
        public static IPAddress iPAddress()
        {
            IPHostEntry iPHostEntry = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress iPAddress = iPHostEntry.AddressList[0];

            for (int i = 0; i < iPHostEntry.AddressList.Length; i++)
            {
                if (iPHostEntry.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                {
                    iPAddress = iPHostEntry.AddressList[i];
                }
            }
            return iPAddress;
        }

        //Establish the remote endpoint for the socket at the controller
        public static IPEndPoint iPEndPoint()
        {
            IPEndPoint _iPEndPoint = new IPEndPoint(iPAddress(), 20201);
            return _iPEndPoint;
        }

        public static CcpMessage AuthCcp()
        {
            CcpMessage ccpMessage = new CcpMessage("100", "AuthoriseReq", "0");
            return ccpMessage;
        }

        public static string StatusReq = @"<CCP MsgID=""102"" MsgName=""StatusReq"" SeqNo=""""><body/></CCP>";
        public static byte[] StatusBytes = Encoding.ASCII.GetBytes(StatusReq);

        public static string AuthoriseReq = "<CCP MsgID=\"100\" MsgName=\"AuthoriseReq\" SeqNo=\"\"><body><ClientID>1234</ClientID> <MACAddress>AABBCCDDEEFF</MACAddress> <ClientType>CUI</ClientType></body></CCP>";
        public static byte[] AuthoriseBytes = Encoding.ASCII.GetBytes(AuthoriseReq);

        public static byte[] AckBytes = Encoding.ASCII.GetBytes("<CCP MsgID=\"10\" MsgName=\"ACK\" SeqNo=\"\"><body/></CCP>");

    }
}
