﻿namespace ManiwaCDM.Utils
{
    class AppUtils
    {
        public static string BtnHelp = "HELP";
        public static string BtnMore = "MORE";
        public static string BtnBack = "BACK";
        public static string BtnCancel = "CANCEL";
        public static string BtnAccept = "ACCEPT";
        public static string BtnDeposit = "DEPOSIT";
        public static string BtnContinue = "CONTINUE";
        public static string BtnFinish = "FINISH";

        public static string Disclaimer = "DISCLAIMER";

        public static string DisclaimerText = "Be advised, transactions greater than KES 999,950.00 will not be credited. If the transaction limit is exceed. Please visit your closest branch to complete the deposit.";

        public static string TermsAndConditions = "TERMS AND CONDITIONS";

        public static string TermsAndConditionsText = "DISCLAIMER\n\nIN RECEIVING AND BUNDLING ITEMS FOR DEPOSIT OR COLLECTION, THIS BANK ACTS ONLY AS ITS CUSTOMERS COLLECTING AGENT AND ASSUMES NO RESPONSIBILITY BEYOND THE EXERCISE OF DUE CARE. ALL ITEMS ARE CREDITED OR CASHED SUBJECT TO FINAL PAYMENT IN CASH OR SOLVENT CREDITS. THIS BANK OR ITS CORRESPONDENTS MAY AS ITS CUSTOMER’S AGENTS, ACCEPTS THE DRAFT OR CREDIT OF ANY BANK OR ANY DRAWEE ACCEPTOR OR PAYOR AS PAYMENT LIEU OF CASH. THE BANK MAY CHARGE FOR ANY ITEM WHETHER RETURNED OR NOT INCLUDING ITEMS DRAWN ON THIS BANK AT ANY TIME BEFORE PAYMENT.\n\nTHE BANK SHALL NOT BE CONSIDERED TO HAVE RECEIVED ITEMS SENT BY MAIL UNTIL THE BANK HAS RECEIVED ACTUAL DELIVERY OF THE SAME FROM THE POST OFFICE.\n\nTHE BANK WILL HAVE RECOURSE TO THE CUSTOMER ON ANY UNPAID/LOST FOREIGN CHEQUES NEGOTIATED FOR WHATEVER REASONS AND FOR AN INDETERMINATE PERIOD";

        public static string MoneyInstructions = "1. Please remove the counted notes from the opening on the counter.\n\n2. Press add more money to start count again.\n\n3. Press cancel to close.";
        public static string BagInstructions = "1.Click Accept drop the counted notes\n2.Click Reject to take back the counted notes.";

    }
}
