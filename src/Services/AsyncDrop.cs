﻿using ManiwaCDM.Data.Models;
using System;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ManiwaCDM.Services
{
    public class AsyncDrop
    {
        BackgroundWorker worker;
        public CcpMessage ccp;
        public DropRequest dropReq;
        public AuthoriseRequest authReq;

        public AsyncDrop()
        {
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = false;
            worker.WorkerReportsProgress = true;
            worker.DoWork += DoWork;
        }

        private void DoWork(object sender, DoWorkEventArgs e)
        {
            IPHostEntry iPHostEntry = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress iPAddress = iPHostEntry.AddressList[0];

            for (int i = 0; i < iPHostEntry.AddressList.Length; i++)
            {
                if (iPHostEntry.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                {
                    iPAddress = iPHostEntry.AddressList[i];
                }
            }
            IPEndPoint iPEndPoint = new IPEndPoint(iPAddress, 20201);

            Console.WriteLine("Start connect to Controller ...");

            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(new IPEndPoint(iPAddress, 20201));

            if (socket.Connected)
            {
                Console.WriteLine("Connected to the controller");
                string dataStr = XmlParser.AuthorizationCcp(new CcpMessage("100", "AuthoriseReq", "0"), authReq);
                byte[] sendBuffer1 = Encoding.ASCII.GetBytes(dataStr);
                socket.Send(sendBuffer1, 0, sendBuffer1.Length, 0);

                while (true)
                {
                    Console.WriteLine("Sending a Drop Cash Request ...");
                    dataStr = XmlParser.DropCashCcp(new CcpMessage("120", "DropRequest", "0"), dropReq);
                    byte[] sendBuffer2 = Encoding.ASCII.GetBytes(dataStr);
                    socket.BeginSend(sendBuffer2, 0, sendBuffer2.Length, SocketFlags.None, ResponseCallback, socket);
                    break;
                }
            }
        }

        void ResponseCallback(IAsyncResult ar)
        {
            //socket.BeginReceive(_recieveBuffer, 0, _recieveBuffer.Length, SocketFlags.None, ReceiveCallback, socket);
            try
            {
                Socket socket = (Socket)ar.AsyncState;
                if (socket != null)
                {
                    if (socket.Connected)
                    {
                        Console.WriteLine("Getting info from controller.");
                        
                    }
                }
                Console.WriteLine("Failed to connect to controller ...");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void CloseSocket()
        {
            Socket socket = null;
            try
            {
                if (socket != null)
                {
                    if (socket.Connected)
                        socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            socket = null;
        }

        public event RunWorkerCompletedEventHandler Completed
        {
            add { worker.RunWorkerCompleted += value; }
            remove { worker.RunWorkerCompleted -= value; }

        }

        public void StartAsync()
        {
            worker.RunWorkerAsync();
        }

        public void Dispose()
        {
            worker.Dispose();
        }
    }
}
