﻿using ManiwaCDM.Data.Models;
using System;
using System.Collections.Generic;
using System.Xml;

namespace ManiwaCDM.Services
{
    class XmlParser
    {

        public static string AuthorizationCcp(CcpMessage ccp, AuthoriseRequest authReq)
        {
            string dataStr = "<CCP MsgID=\"" + ccp.MsgID + "\" MsgName=\"" + ccp.MsgName + "\" SeqNo=\"" + ccp.SeqNo + "\">";
            dataStr += "\n\t<body>";
            dataStr += "\n\t\t<ClientID>" + authReq.ClientID + "</ClientID>";
            dataStr += "\n\t\t<MACAddress>" + authReq.MACAddress + "</MACAddress>";
            dataStr += "\n\t\t<ClientType>" + authReq.ClientType + "</ClientType>";
            dataStr += "\n\t</body>";
            dataStr += "</CCP>";
            return dataStr;
        }

        public static AuthoriseResponse AuthorizationResponse(string responseTxt)
        {
            AuthoriseResponse response = new AuthoriseResponse();
            try
            {
                var xmldoc = new XmlDocument();
                xmldoc.LoadXml(responseTxt);
                var nodes = xmldoc.SelectNodes("/CCP[@MsgID='101']/body");

                XmlNode node = nodes[0];
                response.ClientID = node.ChildNodes[0].InnerText;
                response.Result = node.ChildNodes[1].InnerText;
                response.FailCode = node.ChildNodes[2].InnerText;
                response.SoftwareVersion = node.ChildNodes[3].InnerText;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\nInnerException: " + ex.InnerException);
            }
            
            return response;
        }

        public static StatusReport StatusResponse(string responseTxt)
        {
            StatusReport response = new StatusReport();
            try
            {
                var xmldoc = new XmlDocument();
                xmldoc.LoadXml(responseTxt);
                var nodes = xmldoc.SelectNodes("/CCP[@MsgID='103']/status");

                foreach(XmlNode node in nodes)
                {
                    response.ControllerState = node.ChildNodes[0].InnerText;

                    response.Acceptors = new Acceptor
                    {
                        BA = new BA
                        {
                            Type = node.ChildNodes[1].ChildNodes[0].Attributes[0].InnerText,
                            Status = node.ChildNodes[1].ChildNodes[0].Attributes[1].InnerText,
                            Currency = node.ChildNodes[1].ChildNodes[0].Attributes[2].InnerText
                        },
                        BA1 = new BA()
                        {
                            Type = node.ChildNodes[1].ChildNodes[1].Attributes[0].InnerText,
                            Status = node.ChildNodes[1].ChildNodes[1].Attributes[1].InnerText,
                            Currency = node.ChildNodes[1].ChildNodes[1].Attributes[2].InnerText
                        },
                        CA = new CA()
                        {
                            Type = node.ChildNodes[1].ChildNodes[2].Attributes[0].InnerText,
                            Status = node.ChildNodes[1].ChildNodes[2].Attributes[1].InnerText,
                            Currency = node.ChildNodes[1].ChildNodes[2].Attributes[2].InnerText
                        },
                    };

                    List<Cassette> cassettes = new List<Cassette>();
                    foreach (XmlNode item in node.ChildNodes[2].ChildNodes[0])
                    {
                        Cassette cassette = new Cassette
                        {
                            No = item.Attributes[0].InnerText,
                            Denom = item.Attributes[1].InnerText,
                            Level = item.Attributes[2].InnerText,
                            Capacity = item.Attributes[3].InnerText,
                            Status = item.Attributes[4].InnerText,
                            Type = item.Attributes[5].InnerText,
                        };
                        cassettes.Add(cassette);
                    };
                    response.Dispensers = new Dispenser
                    {
                        NDs = new List<ND>
                    {
                        new ND
                        {
                            Type = node.ChildNodes[2].ChildNodes[0].Attributes[0].InnerText,
                            DeviceID = node.ChildNodes[2].ChildNodes[0].Attributes[1].InnerText,
                            Status = node.ChildNodes[2].ChildNodes[0].Attributes[2].InnerText,
                            CassetteState = node.ChildNodes[2].ChildNodes[0].Attributes[3].InnerText,
                            Currency = node.ChildNodes[2].ChildNodes[0].Attributes[4].InnerText,
                            TotalValue = node.ChildNodes[2].ChildNodes[0].Attributes[5].InnerText,
                            Cassettes = cassettes,
                        }
                    }
                    };

                    List<NoteCount> noteCounts = new List<NoteCount>();
                    foreach (XmlNode item in node.ChildNodes[3].ChildNodes[0])
                    {
                        NoteCount noteCount = new NoteCount
                        {
                            Currency = item.Attributes[0].InnerText,
                            Denomination = item.Attributes[1].InnerText,
                            Count = item.Attributes[2].InnerText,
                        };

                        noteCounts.Add(noteCount);
                    };
                    response.Bag = new Bag
                    {
                        Number = node.ChildNodes[3].Attributes[0].InnerText,
                        Status = node.ChildNodes[3].Attributes[1].InnerText,
                        NoteLevel = node.ChildNodes[3].Attributes[2].InnerText,
                        NoteCapacity = node.ChildNodes[3].Attributes[3].InnerText,
                        ValueLevel = node.ChildNodes[3].Attributes[4].InnerText,
                        ValueCapacity = node.ChildNodes[3].Attributes[5].InnerText,
                        PercentFull = node.ChildNodes[3].Attributes[6].InnerText,
                        NoteCounts = noteCounts,
                    };

                    response.Sensors = new Sensor
                    {
                        Type = node.ChildNodes[4].Attributes[0].InnerText,
                        Status = node.ChildNodes[4].Attributes[0].InnerText,
                        Value = node.ChildNodes[4].Attributes[0].InnerText,
                        Door = node.ChildNodes[4].Attributes[0].InnerText,
                        AuxDoor = node.ChildNodes[4].Attributes[0].InnerText,
                        Bag = node.ChildNodes[4].Attributes[0].InnerText
                    };

                    response.Escrow = new Escrow
                    {
                        Type = node.ChildNodes[5].Attributes[0].InnerText,
                        Status = node.ChildNodes[5].Attributes[1].InnerText,
                        Position = node.ChildNodes[5].Attributes[2].InnerText
                    };

                    response.Transaction = new Transaction
                    {
                        Status = node.ChildNodes[6].ChildNodes[0].InnerText,
                        Type = node.ChildNodes[6].ChildNodes[1].InnerText,
                    };

                    response.CardReader = new CardReader
                    {
                        Type = node.ChildNodes[7].Attributes[0].InnerText,
                        Status = node.ChildNodes[7].Attributes[1].InnerText,
                        ErrorString = node.ChildNodes[7].Attributes[2].InnerText
                    };

                    response.DateTime = node.ChildNodes[8].InnerText;
                }                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\nInnerException: " + ex.InnerException);
            }
            return response;
        }


        public static string DropRequestCcp(CcpMessage ccp, DropRequest dropReq)
        {
            string dataStr = "<CCP MsgID=\"" + ccp.MsgID + "\" MsgName=\"" + ccp.MsgName + "\" SeqNo=\"" + ccp.SeqNo + "\">";
            dataStr += "\n\t<body>";
            dataStr += "\n\t\t<Currency>" + dropReq.Currency + "</Currency>";
            dataStr += "\n\t\t<TransactionValue>" + dropReq.TransactionValue + "</TransactionValue>";
            dataStr += "\n\t\t<InputNumber>" + dropReq.InputNumber + "</InputNumber>";
            dataStr += "\n\t\t<InputSubNumber>" + dropReq.InputSubNumber + "</InputSubNumber>";
            dataStr += "\n\t\t<Reference>" + dropReq.Reference + "</Reference>";
            dataStr += "\n\t\t<ShiftReference>" + dropReq.ShiftReference + "</ShiftReference>";
            dataStr += "\n\t\t<UserID>" + dropReq.UserID + "</UserID>";
            dataStr += "\n\t\t<DropType>" + dropReq.DropType + "</DropType>";
            dataStr += "\n\t</body>";
            dataStr += "</CCP>";
            return dataStr;
        }

        public static DropStatus DropStatusResponse(string responseTxt)
        {
            DropStatus status = new DropStatus();
            try
            {
                var xmldoc = new XmlDocument();
                xmldoc.LoadXml(responseTxt);
                var nodes = xmldoc.SelectNodes("/CCP[@MsgID='121']/body");

                foreach (XmlNode node in nodes)
                {
                    status.Status = node.ChildNodes[0].InnerText;
                    status.TotalValue = node.ChildNodes[1].InnerText;
                    status.TotalCount = node.ChildNodes[2].InnerText;

                    List<NoteCount> noteCounts = new List<NoteCount>();
                    foreach (XmlNode item in node.ChildNodes[3].ChildNodes[0])
                    {
                        NoteCount noteCount = new NoteCount
                        {
                            Currency = item.Attributes[0].InnerText,
                            Denomination = item.Attributes[1].InnerText,
                            Count = item.Attributes[2].InnerText,
                        };

                        noteCounts.Add(noteCount);
                    };

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\nInnerException: " + ex.InnerException);
            }
            return status;
        }
    }


}
