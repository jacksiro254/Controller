﻿using ManiwaCDM.Data.Models;
using ManiwaCDM.Utils;
using System;
using System.ComponentModel;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace ManiwaCDM.Services
{
    // Controller status requester
    public class AsyncStatusReport
    {
        BackgroundWorker worker;
        public AuthoriseRequest authoriseRequest;

        private Socket socket;

        /// The response from the controller.
        private static string response = string.Empty;

        private static ManualResetEvent sendDone = new ManualResetEvent(false);
        private static ManualResetEvent receiveDone = new ManualResetEvent(false);

        public AsyncStatusReport(Socket _socket)
        {
            socket = _socket;
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = false;
            worker.WorkerReportsProgress = true;
            worker.DoWork += DoWork;
        }

        private void DoWork(object sender, DoWorkEventArgs e)
        {
            if (socket.Connected)
            {
                try
                {
                    Console.WriteLine("Sending status request ... ");

                    SendRequest(socket, ApiUtils.StatusReq);
                    sendDone.WaitOne();

                    // Receive the response from the controller.
                    ReceiveRequest(socket);
                    receiveDone.WaitOne();

                    string[] separators = new string[] { "103", "</status></CCP>" };
                    string[] responseFinal = response.Trim().Split(separators, StringSplitOptions.None);

                    //string responseStr 
                    response = "<CCP MsgID=\"103" + responseFinal[1] + "</status></CCP>";

                    // Write the response to the console.
                    Console.WriteLine("Response received:\n {0}", response);

                    // pass the response to the background activity
                    StatusReport statusResp = XmlParser.StatusResponse(response);
                    e.Result = statusResp;
                }
                catch (SocketException ex)
                {
                    e.Cancel = true;
                    Console.WriteLine("Message: " + ex.Message + "\nInnerException: " + ex.InnerException + "\nStackTrace: " + ex.StackTrace);
                }
            }
            else
            {
                Console.WriteLine("Socket not connected");
            }
        }

        private static void SendRequest(Socket socket, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the controller.
            socket.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), socket);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket socket = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = socket.EndSend(ar);
                Console.WriteLine("{0} bytes sent to the controller.", bytesSent);

                // Signal that all bytes have been sent.
                sendDone.Set();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\nInnerException: " + ex.InnerException + "\nStackTrace: " + ex.StackTrace);
            }
        }

        private static void ReceiveRequest(Socket socket)
        {
            try
            {
                // Create the state object.
                StateObject state = new StateObject();
                state.workSocket = socket;

                // Begin receiving the data from the remote device.
                socket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket from the asynchronous state object.
                StateObject state = (StateObject)ar.AsyncState;
                Socket socket = state.workSocket;

                // Read data from the controller.
                int bytesRead = socket.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.
                    response += Encoding.ASCII.GetString(state.buffer, 0, bytesRead).Replace("\n", "").Replace("\t", "").Replace("  ", String.Empty);
                    //Console.WriteLine(response);

                    if (response.Contains("</status></CCP>"))
                    {
                        receiveDone.Set();
                    }
                    else // Get the rest of the data.
                        socket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);                   
                }
                else receiveDone.Set();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\nInnerException: " + ex.InnerException + "\nStackTrace: " + ex.StackTrace);
            }
        }

        public event RunWorkerCompletedEventHandler Completed
        {
            add { worker.RunWorkerCompleted += value; }
            remove { worker.RunWorkerCompleted -= value; }

        }

        public void StartAsync()
        {
            worker.RunWorkerAsync();
        }

        public void Dispose()
        {
            worker.Dispose();
        }
    }
}
