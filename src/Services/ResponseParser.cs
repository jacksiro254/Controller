﻿using ManiwaCDM.Data.Models;

namespace ManiwaCDM.Services
{
    class ResponseParser
    {
        public static CcpMessage AuthorizationResponse(string response)
        {
            CcpMessage message = new CcpMessage();
            var strResponse = response.Split('"');
            message.MsgID = strResponse[1];
            message.MsgName = strResponse[3];
            message.SeqNo = strResponse[5];
            return message;
        }

    }
}
